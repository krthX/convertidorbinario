package org.bryampaniagua.Convertidor;

/**
	Se guarda en una carpeta llamada convertidor y utilizara el archivo ingresar() en Ingresar.
*/

/**
	Esta es la seccion de codigo que recibe un binario y lo convierte a decimal.
*/
import org.bryampaniagua.Ingresar.Ingresar;
public class Convertidor{

	public static void convertir(){
		Ingresar ingresar=new Ingresar();
		String binary=null, binSt=null;
		int decimal=0, cont=0, cont1=0, binInt=0, error=0;				
		
		binary = ingresar.leerDatos();
		
		while(cont1!=binary.length()){
			cont1 = ++cont1; 
			/**binSt=binary.substring(cont,cont1); 
			binInt = Integer.parseInt(binSt); <-- Se pueden utilizar esta porción de código para más comprensión.*/
			binInt=Integer.parseInt(binary.substring(cont,cont1));
			if(binInt==1 || binInt==0){
				decimal = decimal*2+binInt;
			}else{
				error = ++error;
			}
			cont = ++cont; 
		}
		
		if(error==0){
			System.out.println("El binario convertido a decimal : "+decimal);
		}else{
			System.out.println("Este numero no es binario");
		}
		
	}
}