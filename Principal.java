package org.bryampaniagua;

/**
	Es el archivo principal
*/
/**
	-Este codigo instancia a convertir() en Convertidor para que devuelva un valor, hará también una comprobación de caracteres 
		para asegurar que sea un binario.
*/
import org.bryampaniagua.Convertidor.Convertidor;
public class Principal{
	public static void main (String args[]){
		/**
			-Hace una instancia al metódo siguiente.
		*/		
		Convertidor Convertidor = new Convertidor();
		Convertidor.convertir();
			
	}
}
