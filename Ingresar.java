package org.bryampaniagua.Ingresar;

/**
	-Importa las clases necesarias para hacer un ingreso al sistema.
*/

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

public class Ingresar{

	public static String leerDatos(){
		/**
			Se inicializa una variable del tipo BufferedReader que recibira atributos de InputStreamReader que a su vez tambien recibe un atributo de la clase System
		*/
		BufferedReader ing = new BufferedReader(new InputStreamReader(System.in));
		
		String binary = null;
		try{
			System.out.println("Ingrese numero binario");
			binary = ing.readLine();
		}catch(IOException err){
			System.out.println("Error al ingresar dato");
		}
		return binary;
	}
}